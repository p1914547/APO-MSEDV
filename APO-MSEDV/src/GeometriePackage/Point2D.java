package GeometriePackage;

import java.util.*;

/**
 * @class Point 2D avec des Abscisses entre 0 et 1
 */
public class Point2D {
    private float x, y;

    /**
     * Pour créer un point 2D avec des valeurs aléatoires, entre 0 et 1
     */
    public Point2D() {
        Random rand = new Random();
        this.x = rand.nextFloat();
        this.y = rand.nextFloat();
    }

    /**
     * Pour créer un point 2D avec les valeurs passées en paramètres
     * @param x : 0<x<1, abscisse du point
     * @param y : 0<y<1, ordonnée du point
    */
    public Point2D(float x, float y) {
        try {
            if ((x >= 0) && (x <= 1))
                this.x = x;
            else
                throw new Exception("Invalid value for point initialization");
            if ((y >= 0) && (y <= 1))
                this.y = y;
            else
                throw new Exception("Invalid value for point initialization");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Pour changer la position du point 2D avec les valeurs passées en paramètres
     * @param x : 0<x<1, abscisse du point
     * @param y : 0<y<1, ordonnée du point
    */
    public void changePos(float x, float y) {
        if ((x >= 0) && (x <= 1))
            this.x = x;
        if ((y >= 0) && (y <= 1))
            this.y = y;
    }
    /**
     * 
     * @return float Abscisse du point
     */
    public float getX() {
        return x;
    }
    
    /**
     * 
     * @return ordonnée du point
    */
    public float getY() {
        return y;
    }

    /**
     * Calcule la distance euclidienne entre le point appelant et le point passé en paramètre
     * @param p , 2nd point
     * @return float, distance entre les deux points
     */
    public float norme(Point2D p) {
        return (float) Math.sqrt((x - p.getX()) * (x - p.getX()) + (y - p.getY()) * (y - p.getY()));
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }

}
