package PersonnesPackage;
import CustomExceptions.UserIOException;
/**
 * @class Pemet de gérer les personnes de type Candidat
 */
public class Candidat extends Electeur {
    private float score;

    /**
     * Renvoie le score fait à la dernière élection/sondage par le candidat
     * 
     * @return
     */
    public float getScore() {
        return score;
    }

    /**
     * Permet de mettre le score du candidat à l'élection, il doit être compris en 0
     * et 1.
     * 
     * @param score
     */
    public void setScore(float score) {
        if ((score >= 0) )
            this.score = score;
        else
            System.out.println("Score invalide");
    }

    /**
     * Pour créer un candidat avec des caractéristique aléatoires, un nom doit être
     * passé en paramètres
     * 
     * @param nom
     */
    public Candidat(String nom){
        super(nom);
        score = -1;
    }

    /**
     * Pour créer un candidat avec les caractéristique passée en paramètres
     * 
     * @param pwrAchat , float, Score pouvoir d'achat (entre 0 et 1)
     * @param ecolo    , float, Score écologie (entre 0 et 1)
     * @param nom
     */
    public Candidat(float pwrAchat, float ecolo, String nom) throws UserIOException{
        super(pwrAchat, ecolo, nom);
        score = -1;
    }

    /**
     * @return String
     */
    public String toString() {
        String res = super.toString();
        if ((score >= 0) && (score < 1)) {
            res += "\t score  : " + score *100 + "%";
        }
        else{
            res += "\t score  : " + score;
            
        }
        return res;
    }

    /**
     * renvoie vrai si le score du candidat qui appelle la fonciton est plus petit
     * que le score du candidat passé en paramètres
     * 
     * @param cand
     * @return
     */
    public boolean inferieur(Candidat cand) {
        return this.score < cand.getScore();
    }

    /**
     * Méthode compareTo des comparables sauf qu'elle renvoie l'inverse de ce qui
     * est fait habituellement.
     * En effet, elle renvoie 1 si a<b et -1 si a >b, avec a notre candidat et b
     * l'autre candidat
     * 
     * @param o
     * @return
     */
    public int compareTo(Electeur o) {
        int res1 = super.compareTo(o);
        if (o instanceof Candidat) {
            return this.compareTo((Candidat) o);
        }
        return res1;
    }

    public int compareTo(Candidat o) {
        return (this.score < o.getScore()) ? 1 : ((this.score == o.getScore()) ? 0 : -1);
    }
}
