package PersonnesPackage;

import java.util.ArrayList;
import CustomExceptions.UserIOException;

/**
 * @class Pemet de gérer les personnes de type Electeur
 */
public class Electeur extends GeometriePackage.Point2D implements Comparable<Electeur> {
    protected ArrayList<Electeur> tabProches = null;
    protected int nbproches;
    private String nom;
    public static ArrayList<String> nomUtilises = null;

    /**
     * 
     * @return String nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return Electeur[]
     */
    public ArrayList<Electeur> getTabProches() {
        return tabProches;
    }

    /**
     * @return int
     */
    public int getNbproches() {
        return nbproches;
    }

    /**
     * Pour créer un électeur avec des caractéristique aléatoires, un nom doit être
     * passé en paramètres
     * 
     * @param nom
     */
    public Electeur(String nom) {
        super();
        if (null == nomUtilises) {
            nomUtilises = new ArrayList<String>();
        }
        if (!nomUtilises.contains(nom)) {
            tabProches = new ArrayList<Electeur>();
            nbproches = 0;
            this.nom = nom;
            nomUtilises.add(nom);
        }
    }

    /**
     * Pour créer un électeur avec les caractéristique passée en paramètres, un nom
     * doit être passé en paramètres
     * 
     * @param pwrAchat , float, Score pouvoir d'achat (entre 0 et 1)
     * @param ecolo    , float, Score écologie (entre 0 et 1)
     * @param nom
     */
    public Electeur(float pwrAchat, float ecolo, String nom) throws UserIOException{
        super(pwrAchat, ecolo);
        if (null == nomUtilises) {
            nomUtilises = new ArrayList<String>();
        }
        if (!nomUtilises.contains(nom)) {
            tabProches = new ArrayList<Electeur>();
            nbproches = 0;
            this.nom = nom;
            nomUtilises.add(nom);
        }else{
            throw new UserIOException("Nom déjà utilisé !");
        }
    }

    /**
     * @return String
     */
    public String toString() {
        String res = "nom : " + nom + '\t';
        res += " position : " + super.toString();
        return res;
    }

    /**
     * permet d'ajouter des proches à l'électeur ce qui peut faire évoluer ses
     * opinions
     * 
     * @param proche
     * @return boolean, indique si le proche a pû être ajouté ou non
     */
    public boolean AjouterProches(Electeur proche) {
        if (!tabProches.contains(proche)) {

            tabProches.add(proche);
            nbproches++;
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int compareTo(Electeur o) {
        return nom.compareTo(o.getNom());
    }

}
