package CustomExceptions;

public class UserIOException extends Exception {
    public UserIOException(){
        super();
    }

    public UserIOException(String message){
        super(message);
    }
    
}
