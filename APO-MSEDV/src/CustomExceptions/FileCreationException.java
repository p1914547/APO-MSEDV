package CustomExceptions;

public class FileCreationException extends Exception {
    public FileCreationException(){
        super();
    }

    public FileCreationException(String message){
        super(message);
    }
    
}
