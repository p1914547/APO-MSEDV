package MenuPackage;

import ScrutinPackage.*;
import PersonnesPackage.*;

public abstract class Menu {
    protected Scrutin modeActuel = null;
    protected boolean exitProgram = false;
    protected Electeur elecs[] = null;
    protected Candidat candid[] = null;
    protected boolean entourage = false;


    /**
     * Une fois appelée, cette fonction lance le programme jusqu'à qu'il soit stoppé
     */
    public void runProg() {
        while (!exitProgram) {
            MenuPrincipal();
        }

    }

    /**
     * Affiche le menu principal de l'application avec les différentes actions
     * possibles
     */
    protected abstract void MenuPrincipal();

    /**
     * Permet à l'utilisateur d'initialiser des candidats
     */
    protected abstract void initialiserCandidats();

    /**
     * Permet à l'utilisateur d'initialiser des électeurs
     */
    protected abstract void initialiserElecteurs();

    /**
     * Permet à l'utilisateur de choisir un mode de scrutin
     */
    protected abstract void choisirScrutin();

    /**
     * Permet à l'utilisateur de lancer un vote ou un sondage
     * 
     * @param type permet de chosir entre Vote (true) et Sondage (false)
     */
    protected abstract void lancerSimulation(boolean type);

    /**
     * Permet de modifier la liste des électeurs à la volée entre deux simulations, sans changer le type de simulation.
     */
    protected abstract void modifierElecteurs();
    
    /**
     * Permet de modifier la liste des candidats à la volée entre deux simulations, sans changer le type de simulation.
     */
    protected abstract void modifierCandidats();

    /**
     * Permet de générer un entourage à l'utilisateur, soit de manière aléatoire, soit avec des électeurs définies par l'utilisateur
     */
    protected abstract void avecEntourage();

}
