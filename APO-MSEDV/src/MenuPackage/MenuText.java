package MenuPackage;

import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import PersonnesPackage.*;
import ScrutinPackage.*;
import CustomExceptions.*;

public final class MenuText extends Menu {

    @Override
    protected void MenuPrincipal() {
        System.out.println("Que voulez vous faire ?");

        System.out.println("0. Quitter");
        System.out.println("1. Choisir un mode de scrutin");

        if (modeActuel != null) {
            System.out.println("2. Lancer un sondage");
            System.out.println("3. Lancer une élection");
            System.out.println("4. Modifier les candidats");
            System.out.println("5. Modifier les électeurs");
            System.out.println("6. Interactions Socio-Politiques");
            boolean valeurOk = true;
            do {
                int inVal = lectureIntClavier();
                switch (inVal) {
                    case 0:
                        exitProgram = true;
                        break;
                    case 1:
                        choisirScrutin();
                        valeurOk = true;
                        break;
                    case 2:
                        lancerSimulation(false);
                        valeurOk = true;
                        break;
                    case 3:
                        lancerSimulation(true);
                        valeurOk = true;
                        break;
                    case 4:
                        modifierCandidats();
                        valeurOk = true;
                        break;
                    case 5:
                        modifierElecteurs();
                        valeurOk = true;
                        break;
                    case 6:
                        ModifyPos();
                        valeurOk = true;
                        break;
                    default:
                        System.out.println("Entrez une des valeurs ci-dessus !");
                        valeurOk = false;
                        break;
                }
            } while (!valeurOk && !exitProgram);
        } else {
            boolean valeurOk = true;
            do {
                int inVal = lectureIntClavier();
                switch (inVal) {
                    case 0:
                        exitProgram = true;
                        break;
                    case 1:
                        choisirScrutin();
                        valeurOk = true;
                        break;

                    default:
                        System.out.println("Entrez une des valeurs ci-dessus !");
                        valeurOk = false;
                        break;
                }
            } while (!valeurOk && !exitProgram);
        }

    }

    /**
     * Lit l'entrée entière au clavier et gère les exceptions
     * 
     * @return int : l'entier lu au clavier
     */
    private int lectureIntClavier() {
        int intInValue = -1;
        String inValue;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            inValue = br.readLine();
            intInValue = Integer.parseInt(inValue);

        } catch (Exception e) {
            System.out.println("La valeur rentrée n'est pas un entier");
        }

        return intInValue;
    }

    /**
     * Lit l'entrée entière au clavier et gère les exceptions
     * 
     * @return float : réel lu au clavier
     */
    private float lecturefloatClavier() {
        float floatInValue = -1;
        String inValue;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            inValue = br.readLine();
            floatInValue = Float.parseFloat(inValue);
            if ((floatInValue < 0) && (floatInValue > 1)) {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("La valeur rentrée n'est pas un réel");
        }

        return floatInValue;
    }

    /**
     * Lit l'entrée entière au clavier et gère les exceptions
     * 
     * @return String : chaîne de caractères lu au clavier
     */
    private String lectureStringClavier() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inValue = "Erreur";
        try {
            inValue = br.readLine();
            if (inValue.matches("[+-]?\\d*(\\.\\d+)?")) {
                inValue = "Erreur";
                throw new Exception("Nombre Invalide");
            }
        } catch (Exception e) {
            System.out.println("Problème lors de la lecture clavier");
        }

        return inValue;
    }

    @Override
    protected void initialiserCandidats() {
        if (candid == null) {
            System.out.println(
                    "Voulez-vous initialiser les données des candidats aléatoirement ou en entrant les données à la main ?");
            System.out.println("1. remplissage aléatoire \n2. remplissage à la main");
            boolean valeurOk = true;
            int inVal1 = -1;
            int inVal2 = -1;
            do {
                inVal1 = lectureIntClavier();
                if ((inVal1 <= 2) && (inVal1 > 0))
                    valeurOk = true;
                else {
                    valeurOk = false;
                    System.out.println("Entrez une des valeurs ci-dessus !");
                }
            } while (!valeurOk && !exitProgram);
            System.out.println("Combien voulez-vous initialiser de personne ?");
            do {
                inVal2 = lectureIntClavier();
                if (inVal2 > 0)
                    valeurOk = true;
                else {
                    valeurOk = false;
                    System.out.println("Entrez une valeur strictement positive !");
                }
            } while (!valeurOk && !exitProgram);
            candid = new Candidat[inVal2];
            for (int i = 0; i < inVal2; i++) {
                if (inVal1 == 1) {
                    candid[i] = new Candidat(String.valueOf(i));
                } else if (inVal1 == 2) {
                    System.out.println("Candidat n°" + String.valueOf(i + 1));
                    System.out.println("Entrez la valeur du pouvoir d'achat (entre 0 et 1)");
                    float pwr;
                    do {
                        pwr = lecturefloatClavier();
                        if ((pwr > 1) || (pwr < 0))
                            System.out.println("valeur incorrect");
                    } while ((pwr > 1) || (pwr < 0));
                    System.out.println("Entrez la valeur d'écologie du candidat (entre 0 et 1)");
                    float ecolo;
                    do {
                        ecolo = lecturefloatClavier();
                        if ((ecolo > 1) || (ecolo < 0))
                            System.out.println("valeur incorrect");
                    } while ((ecolo > 1) || (ecolo < 0));
                    System.out.println("Entrez le nom du candidat");
                    String nom;
                    do {
                        valeurOk = true;
                        try {
                            do {
                                nom = lectureStringClavier();
                                if (nom == "Erreur") {
                                    System.out.println("nom incorrect");
                                }
                            } while (nom == "Erreur");
                            candid[i] = new Candidat(pwr, ecolo, nom);
                        } catch (UserIOException e) {
                            valeurOk = false;
                            System.out.println(e.getMessage());

                        }
                    } while (!valeurOk);
                }
                System.out.println("Candidat n°" + String.valueOf(i + 1) + " créé !");
            }
        }

    }

    @Override
    protected void initialiserElecteurs() {
        if (elecs == null) {
            System.out.println(
                    "Voulez-vous initialiser les données des électeurs aléatoirement ou en entrant les données à la main ?");
            System.out.println("1. remplissage aléatoire \n2. remplissage à la main");
            boolean valeurOk = true;
            int inVal1 = -1;
            int inVal2 = -1;
            do {
                inVal1 = lectureIntClavier();
                if ((inVal1 <= 2) && (inVal1 > 0))
                    valeurOk = true;
                else {
                    valeurOk = false;
                    System.out.println("Entrez une des valeurs ci-dessus !");
                }
            } while (!valeurOk && !exitProgram);
            System.out.println("Combien voulez-vous initialiser de personne ?");
            do {
                inVal2 = lectureIntClavier();
                if (inVal2 > 0)
                    valeurOk = true;
                else {
                    valeurOk = false;
                    System.out.println("Entrez une valeur strictement positive !");
                }
            } while (!valeurOk && !exitProgram);
            elecs = new Electeur[inVal2];

            for (int i = 0; i < inVal2; i++) {
                if (inVal1 == 1) {
                    elecs[i] = new Electeur(String.valueOf(i + candid.length));
                } else if (inVal1 == 2) {
                    System.out.println("Candidat n°" + String.valueOf(i + 1));
                    System.out.println("Entrez la valeur du pouvoir d'achat (entre 0 et 1)");
                    float pwr;
                    do {
                        pwr = lecturefloatClavier();
                        if ((pwr > 1) || (pwr < 0))
                            System.out.println("valeur incorrect");
                    } while ((pwr > 1) || (pwr < 0));
                    System.out.println("Entrez la valeur d'écologie de l'électeur (entre 0 et 1)");
                    float ecolo;
                    do {
                        ecolo = lecturefloatClavier();
                        if ((ecolo > 1) || (ecolo < 0))
                            System.out.println("valeur incorrect");
                    } while ((ecolo > 1) || (ecolo < 0));
                    System.out.println("Entrez le nom de l'électeur :");
                    String nom;
                    do {
                        valeurOk = true;
                        try {
                            do {
                                nom = lectureStringClavier();
                                if (nom == "Erreur") {
                                    System.out.println("nom incorrect");
                                }
                            } while (nom == "Erreur");
                            elecs[i] = new Electeur(pwr, ecolo, nom);
                        } catch (UserIOException e) {
                            valeurOk = false;
                            System.out.println(e.getMessage());

                        }
                    } while (!valeurOk);
                }
                System.out.println("Electeur n°" + String.valueOf(i + 1) + " créé !");

            }
            Electeur temp[] = new Electeur[elecs.length + candid.length];
            System.arraycopy(candid, 0, temp, 0, candid.length);
            System.arraycopy(elecs, 0, temp, candid.length, elecs.length);
            elecs = temp;
            avecEntourage();
        }

    }

    @Override
    protected void choisirScrutin() {
        initialiserCandidats();
        initialiserElecteurs();
        System.out.println("Quel type de scrutin voudrez-vous effectuer ?");
        System.out.println("1. Scrutin majoritaire en 1 tour");
        System.out.println("2. Scrutin majoritaire en 2 tours");
        System.out.println("3. Vote par approbation");
        System.out.println("4. Vote Alternatif");
        System.out.println("5. Vote selon la méthode de borda");
        boolean valeurOk = true;
        do {
            int inVal = lectureIntClavier();
            switch (inVal) {
                case 0:
                    exitProgram = true;
                    break;
                case 1:
                    modeActuel = new Scrutin_1_Tour(candid, elecs);
                    valeurOk = true;
                    break;
                case 2:
                    modeActuel = new Scrutin_2_Tours(candid, elecs);
                    valeurOk = true;
                    break;
                case 3:
                    modeActuel = new Approbation(candid, elecs);
                    valeurOk = true;
                    break;
                case 4:
                    modeActuel = new Alternatif(candid, elecs);
                    valeurOk = true;
                    break;
                case 5:
                    modeActuel = new Borda(candid, elecs);
                    valeurOk = true;
                    break;
                default:
                    System.out.println("Entrez une des valeurs ci-dessus !");
                    valeurOk = false;
                    break;
            }
        } while (!valeurOk && !exitProgram);

    }

    @Override
    protected void lancerSimulation(boolean type) {
        if (!type) {
            System.out.println("Lancement d'un sondage de la population");
            boolean valeurOk = true;
            int pourcent = 10;
            do {
                System.out
                        .println("Quel pourcentage de la population voulez-vous interroger ? (valeur entre 0 et 100)");
                pourcent = lectureIntClavier();
                if ((pourcent >= 0) && (pourcent <= 100))
                    valeurOk = true;
                else
                    valeurOk = false;
            } while (!valeurOk);
            modeActuel.CalculateSondage(pourcent);
            System.out.println("Voulez-vous faire évoluer les opinions suite au sondage ?");
            System.out.println(
                    "1. Non \n2. Oui, Avec un déplacement simple\n3. Oui avec un déplacement par utilité vers 1 candidat");
            System.out.println("4. Oui, avec un déplacement par utilité vers tous les candidats");
            do {
                int inVal = lectureIntClavier();
                switch (inVal) {
                    case 0:
                        exitProgram = true;
                        break;
                    case 1:
                        valeurOk = true;
                        break;
                    case 2:
                        modeActuel.ModifyPosApresSondage(1);
                        valeurOk = true;
                        break;
                    case 3:
                        modeActuel.ModifyPosApresSondage(2);
                        valeurOk = true;
                        break;
                    case 4:
                        modeActuel.ModifyPosApresSondage(3);
                        valeurOk = true;
                        break;
                    default:
                        System.out.println("Entrez une des valeurs ci-dessus !");
                        valeurOk = false;
                        break;
                }
            } while (!valeurOk && !exitProgram);

        } else {
            System.out.println("Lancement du vote !");
            modeActuel.CalculateScrutin();

        }
        System.out.println(modeActuel.displayResult());
        boolean valeurOk = true;
        System.out.println("Voulez-vous sauvegarder les résultats du sondage ?");
        System.out.println("1. Oui \n2. Non");
        valeurOk = true;
        do {
            int inVal = lectureIntClavier();
            switch (inVal) {
                case 0:
                    exitProgram = true;
                    break;
                case 1:
                    System.out.println(modeActuel.csvSave());
                    valeurOk = true;
                    break;
                case 2:
                    valeurOk = true;
                    break;
                default:
                    System.out.println("Entrez une des valeurs ci-dessus !");
                    valeurOk = false;
                    break;
            }
        } while (!valeurOk && !exitProgram);

    }

    @Override
    protected void modifierElecteurs() {
        Candidat.nomUtilises.clear();
        elecs = null;
        initialiserElecteurs();
        modeActuel.SetNewValue(candid, elecs);
    }

    @Override
    protected void modifierCandidats() {
        for (Candidat C : candid) {
            Candidat.nomUtilises.remove(C.getNom());
        }
        elecs = Arrays.copyOfRange(elecs, candid.length, elecs.length);
        candid = null;
        initialiserCandidats();
        Electeur temp[] = new Electeur[elecs.length + candid.length];
        System.arraycopy(candid, 0, temp, 0, candid.length);
        System.arraycopy(elecs, 0, temp, candid.length, elecs.length);
        elecs = temp;
        modeActuel.SetNewValue(candid, elecs);
    }

    @Override
    protected void avecEntourage() {
        System.out.println("Voulez-vous générer un entourage pour les personnes ? (électeurs et candidats)");
        System.out.println("1. Oui \n2. Non");
        boolean valeurOk = true;
        do {
            int inVal = lectureIntClavier();
            switch (inVal) {
                case 0:
                    exitProgram = true;
                    break;
                case 1:
                    entourage = true;
                    valeurOk = true;
                    break;
                case 2:
                    valeurOk = true;
                    break;
                default:
                    System.out.println("Entrez une des valeurs ci-dessus !");
                    valeurOk = false;
                    break;
            }
        } while (!valeurOk && !exitProgram);
        if (entourage) {
            System.out.println("Combien voulez-vous initialiser de proches pour chaque personne ? (min : 1, max : "
                    + String.valueOf(elecs.length) + " ils seront générés aléatoirement)");
            int nbProches;
            do {
                nbProches = lectureIntClavier();
                if ((nbProches >= 2) && (nbProches < elecs.length))
                    valeurOk = true;
                else {
                    valeurOk = false;
                    System.out.println("Entrez une valeur entre le min et le max ci-dessus !");
                }
            } while (!valeurOk && !exitProgram);

            Random rand = new Random();
            for (Electeur e : elecs) {
                for (int i = 0; i < nbProches; i++) {
                    boolean noSuccess;
                    do {
                        int random = rand.nextInt(elecs.length);
                        noSuccess = !e.AjouterProches(elecs[random]);
                    } while (noSuccess);
                }
            }

        }

    }

    /**
     * Appelle la fonction d'interactions socios-politiques au nombre de pas n demandé par l'utilisateur
     */
    private void ModifyPos() {
        int inVal2 = -1;
        boolean valeurOk2 = true;
        System.out.println("Combien de pas voulez vous faire ?");
        do {
            inVal2 = lectureIntClavier();
            if (inVal2 > 0)
                valeurOk2 = true;
            else {
                valeurOk2 = false;
                System.out.println("Entrez une valeur strictement positive !");
            }
        } while (!valeurOk2 && !exitProgram);
        for (int i = 0; i < inVal2; i++) {
            modeActuel.ModifyPos(entourage);
        }
        System.out.println("Les électeurs ont eu " + inVal2 + " interactions sociales");
    }

}
