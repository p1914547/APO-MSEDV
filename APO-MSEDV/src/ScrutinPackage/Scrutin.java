package ScrutinPackage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.io.File;
import java.io.FileWriter;
import java.util.*;

import CustomExceptions.FileCreationException;
import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

/**
 * @class Classe définissant la majorité des fonctions nécessaires au bon
 *        fonctionnement d'un scrutin ou d'un sondage
 */
public abstract class Scrutin implements Sondage {

    protected Candidat resultat[];
    protected Candidat candidats[];
    protected int scores[];
    protected Electeur electeurs[];

    public Candidat[] getResultat() {
        return resultat;
    }

    /**
     * Classe abstraite, ce constructeur n'est utilisé que par les classes fille
     * pour éviter la redondance de code
     * Crée un nouveau scrutin avec les listes des électeurs et des candidats en
     * paramètres
     * 
     * @param candidats
     * @param electeurs
     */
    public Scrutin(Candidat[] candidats, Electeur[] electeurs) {
        this.candidats = candidats;
        this.resultat = candidats;
        this.electeurs = electeurs;
    }

    /**
     * Permet de rentrer des nouveaux tableaux de candidats après la création du
     * type de scrutin
     * 
     * @param candidats
     * @param electeurs
     */
    public void SetNewValue(Candidat[] candidats, Electeur[] electeurs) {
        this.candidats = candidats;
        this.resultat = candidats;
        this.electeurs = electeurs;
    }

    @Override
    public void CalculateSondage(int Pourcentage) {
        Random rand = new Random();
        Electeur elecSonde[] = new Electeur[(int) electeurs.length * Pourcentage / 100];
        for (int i = 0; i < elecSonde.length; i++) {
            elecSonde[i] = electeurs[rand.nextInt(electeurs.length)];
        }
        CalculateScrutin();
    }

    @Override
    public void ModifyPos(boolean AvecEntourage) {
        Random rand = new Random();
        int talkTo;
        float dist;
        ArrayList<Electeur> possibles = null;
        for (int i = 0; i < electeurs.length; i++) {
            if (AvecEntourage) {
                possibles = electeurs[i].getTabProches();
            } else {
                possibles = new ArrayList<Electeur>(Arrays.asList(electeurs));
            }
            do {
                if (AvecEntourage) {
                    talkTo = rand.nextInt(electeurs[i].getNbproches());
                } else {
                    talkTo = rand.nextInt(electeurs.length);
                }
            } while (talkTo == i);
            dist = electeurs[i].norme(possibles.get(talkTo));
            if (dist > 0.75) {
                electeurs[i].changePos(electeurs[i].getX() + (possibles.get(talkTo).getX() - electeurs[i].getX()) / 4,
                        electeurs[i].getY() + (possibles.get(talkTo).getY() - electeurs[i].getY()) / 4);
            } else if (dist < 0.25) {
                electeurs[i].changePos(electeurs[i].getX() - (possibles.get(talkTo).getX() - electeurs[i].getX()) / 4,
                        electeurs[i].getY() - (possibles.get(talkTo).getY() - electeurs[i].getY()) / 4);
            }
        }

    }

    @Override
    public void ModifyPosApresSondage(int mode) {
        try {
            if (mode == 1) {
                modifyPosSonMode1();
            } else if (mode == 2) {
                modifyPosSonMode2();
            } else {
                modifyPosSonMode3();
            }
        } catch (Exception e) {
            System.out.println("Erreur lors de la modification de position");
        }
    }

    /**
     * Exécute le vote sur la population d'électeurs et de candidats passés en
     * paramètres et écrit le résultat dans le tableau Resultat,
     * où le gagnant de l'élection est dans la première case, le second dans la
     * seconde ...
     * 
     * @param Electeurs : population d'électeurs
     * @param Candidats : Candidats à l'élection
     */
    public abstract void CalculateScrutin();

    /**
     * En cas d'égalité entre les deux premiers candidats, on refait le vote avec
     * seulement 90% de la population
     */
    protected void verifierEgalite() {
        if (resultat.length > 1) {
            if (resultat[0].compareTo(resultat[1]) == 0) {
                Electeur tempElec[] = Arrays.copyOf(electeurs, electeurs.length);
                electeurs = Arrays.copyOf(tempElec, 4 * electeurs.length / 5);
                CalculateScrutin();
                electeurs = tempElec;
            }
        }
    }

    /**
     * retourne la chaîne de caractères avec le résulat de l'élection ou du sondage
     * précédent
     * 
     * @return String
     */
    public String displayResult() {
        String res = "Voici les scores de chaque candidat ainsi que leur positionnement : \n";
        for (Candidat can : resultat) {
            res += can.toString() + "\n";
        }
        return res;
    }

    /**
     * retourne la chaîne de caractères avec le positionnement actuel des électeurs
     * 
     * @return String
     */
    public String displayPos() {
        String res = "Voici le positionnement de chaque électeur : \n";
        for (Electeur e : electeurs) {
            res += e.toString() + "\n";
        }
        return res;
    }

    /**
     * Cette fonction permet de rapprocher chaque électeur du candidat qui est le
     * plus proche de lui
     * C'est le premier mode de rapprochement après un sondage
     */
    private void modifyPosSonMode1() {
        try {
            for (int j = 0; j < electeurs.length; j++) {
                float max = 1;
                int maxIndex = 0;
                PersonnesPackage.Electeur e = electeurs[j];
                for (int i = 0; i < candidats.length; i++) {
                    float n = e.norme(candidats[i]);
                    if (n < max) {
                        max = n;
                        maxIndex = i;
                    }
                }
                scores[maxIndex] += 1;
                electeurs[j].changePos(electeurs[j].getX() - (resultat[maxIndex].getX() - electeurs[j].getX()) / 4,
                        electeurs[j].getY() - (resultat[maxIndex].getY() - electeurs[j].getY()) / 4);
            }
        } catch (Exception e) {
            System.out.println("Erreur lors de la modification de position");
        }

    }

    /**
     * Cette fonction permet de rapprocher chaque électeur du candidat qui a
     * l'utilité la plus élevée
     * L'utilité est l'inverse de distance entre le candidat et l'électeur multiplié
     * par le pourcentage de voix dans le sondage
     * C'est le deuxième mode de rapprochement après un sondage
     */
    private void modifyPosSonMode2() {
        try {
            for (int j = 0; j < electeurs.length; j++) {
                float max = 1;
                int maxIndex = 0;
                PersonnesPackage.Electeur e = electeurs[j];
                for (int i = 0; i < candidats.length; i++) {
                    float n = e.norme(candidats[i]);
                    n = candidats[i].getScore() / n;
                    if (n > max) {
                        max = n;
                        maxIndex = i;
                    }
                }
                electeurs[j].changePos(electeurs[j].getX() - (resultat[maxIndex].getX() - electeurs[j].getX()) / 4,
                        electeurs[j].getY() - (resultat[maxIndex].getY() - electeurs[j].getY()) / 4);
            }
        } catch (Exception e) {
            System.out.println("Erreur lors de la modification de position");
        }

    }

    /**
     * Cette fonction permet de rapprocher chaque électeur des candidats en fonction
     * de leur utilité
     * L'utilité est l'inverse de distance entre le candidat et l'électeur multiplié
     * par le pourcentage de voix dans le sondage
     * C'est le deuxième mode de rapprochement après un sondage
     */
    private void modifyPosSonMode3() {
        try {
            HashMap<String, Float> utilite = new HashMap<String, Float>();
            for (int j = 0; j < electeurs.length; j++) {
                utilite.clear();
                for (int i = 0; i < resultat.length; i++) {
                    utilite.put(candidats[i].getNom(), (float) resultat[i].norme(candidats[i]));
                }
                for (int i = 0; i < resultat.length; i++) {
                    electeurs[j].changePos(
                            electeurs[j].getX()
                                    - (resultat[i].getX() - electeurs[j].getX()) * utilite.get(resultat[i].getNom()),
                            electeurs[j].getY()
                                    - (resultat[i].getY() - electeurs[j].getY()) * utilite.get(resultat[i].getNom()));
                }
            }
        } catch (Exception e) {
            System.out.println("Erreur lors de la modification de position");
        }
    }

    /**
     * Sauvegarde le résultat du dernier sondage dans un fichier csv, dans le
     * dossier saves
     */
    public String csvSave() {
        String res = "";
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");
            String filename = "saves/" + this.getClass().getName() + "_" + dtf.format(LocalDateTime.now()) + ".csv";
            File myFile = new File(filename);
            if (myFile.createNewFile()) {
                res += "Fichier créé : " + myFile.getName() + "\n";
            } else {
                throw new FileCreationException("File already exists.");
            }
            FileWriter myWriter = new FileWriter(filename);
            myWriter.write("Nom de la personne" + "; " + "Position Pouvoir Achat" + "; " + "Position ecologie" + "; "
                    + "Score du candidat" + "; Type de bulletin :" + this.getClass().getName() + " \n");
            for (int i = 0; i < candidats.length; i++) {
                myWriter.write(candidats[i].getNom() + "; " + candidats[i].getX() + "; " + candidats[i].getY() + "; "
                        + candidats[i].getScore() + "; \n");
            }
            for (int i = candidats.length; i < electeurs.length; i++) {
                myWriter.write(
                        electeurs[i].getNom() + "; " + electeurs[i].getX() + "; " + electeurs[i].getY() + "; \n");
            }
            myWriter.close();
            res += "Sauvegarde effectuée avec succès !";
        } catch (FileCreationException e) {
            res += '\n' + e.getMessage();
        } catch (IOException e) {
            res += '\n' + e.getMessage();
        } catch (Exception e) {
            res += '\n' + e.getMessage();
        }
        return res;
    }

}
