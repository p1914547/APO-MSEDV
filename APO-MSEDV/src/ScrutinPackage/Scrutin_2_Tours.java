package ScrutinPackage;

import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

public final class Scrutin_2_Tours extends Scrutin {

    public Scrutin_2_Tours(Candidat[] candidats, Electeur[] electeurs) {
        super(candidats, electeurs);
    }

    @Override
    public void CalculateScrutin() {
        Scrutin_1_Tour scrut1 = new Scrutin_1_Tour(candidats, electeurs);
        scrut1.CalculateScrutin();
        resultat = scrut1.getResultat();
        Candidat candTemp[];
        if (resultat.length > 1) {
            candTemp = new Candidat[] { resultat[0], resultat[1] };
        } else {
            candTemp = new Candidat[] { resultat[0] };
        }
        scrut1.SetNewValue(candTemp, electeurs);
        scrut1.CalculateScrutin();
        System.arraycopy(scrut1.getResultat(), 0, resultat,0 , scrut1.getResultat().length);
    }


    @Override
    public String displayResult(){
        String res = super.displayResult();
        res += "Le score est celui du tour auquel le candidat a participé en dernier : pour les deux premiers, le 2nd tour, sinon le premier tour";
        return res;
    }

}
