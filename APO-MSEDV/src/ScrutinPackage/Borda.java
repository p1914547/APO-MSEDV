package ScrutinPackage;

import java.util.Arrays;
import java.util.HashMap;
import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

public final class Borda extends Scrutin {

    public Borda(Candidat[] candidats, Electeur[] electeurs) {
        super(candidats, electeurs);
    }

    @Override
    public void CalculateScrutin() {
        resultat = candidats;
        HashMap<String, Float> scoresActuels =new HashMap<String, Float>();
        for (int i = 0; i < candidats.length; i++) {
            resultat[i].setScore(0);
        }
        for (int j = 0; j < electeurs.length; j++) {
            scoresActuels.clear();
            for (int i = 0; i < candidats.length; i++) {
                scoresActuels.put(candidats[i].getNom(), (float) candidats[i].getScore());
                resultat[i].setScore(electeurs[j].norme(candidats[i]));
            }
            Arrays.parallelSort(resultat);
            for(int i=0; i<resultat.length; i++) {
                resultat[i].setScore((float) scoresActuels.get(resultat[i].getNom()) +resultat.length - i);
            }
        }
        Arrays.parallelSort(resultat);
        verifierEgalite();
        int somme = 0;
        
        for(int i = 0; i < resultat.length; i++){
            somme += resultat[i].getScore();
        }
        for(int i = 0; i < resultat.length; i++){
            resultat[i].setScore(resultat[i].getScore()/somme);
        }        
    }

}
