package ScrutinPackage;

/**
 * @interface Permet de donner ce type aux scrutins et de définir les noms de fonctions apellable pour un scrutin
 */
public interface Sondage {

    /**
     * Exécute le sondage sur la population d'électeurs et de candidats passés en paramètres et écrit le résultat dans le tableau resultat,
     * où le gagnant du sondage est dans la première case, le second dans la seconde ...
     * @param Pourcentage : Pourcentage de la population à prendre en compte lors du sondage (entier entre 0 et 100)
     */
    public void CalculateSondage(int Pourcentage);

    /**
     * Modifie la position des électeurs en paramètres en fonction des résultats du précédent sondage / de la précédente élection.
     * @param AvecEntourage : si vrai, prend en compte l'entourage de la personne, sinon, le changement s'effectue avec une personne au hasard dans la population
     */
    public void ModifyPos(boolean AvecEntourage);

    /**
     * Permet de modifier la position des électeurs après sondage en fonction des résultats
     * @param mode
     */
    public void ModifyPosApresSondage(int mode);

    public String csvSave();
}
