package ScrutinPackage;

import java.util.Arrays;

import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

/**
 * 
 * Définition d'un scrutin majoritaire à 1 tour
 */
public final class Scrutin_1_Tour extends Scrutin {

    /**
     * Crée un nouveau scrutin avec les listes des électeurs et des candidats en
     * paramètres
     * 
     * @param candidats
     * @param electeurs
     */
    public Scrutin_1_Tour(Candidat[] candidats, Electeur[] electeurs) {
        super(candidats, electeurs);
    }

    @Override
    public void CalculateScrutin() {
        scores = new int[candidats.length];
        Arrays.fill(scores, 0);
        resultat = Arrays.copyOf(candidats, candidats.length);
        for (int j = 0; j < electeurs.length; j++) {
            float max = 1;
            int maxIndex = 0;
            PersonnesPackage.Electeur e = electeurs[j];
            for (int i = 0; i < candidats.length; i++) {
                float n = e.norme(candidats[i]);
                if (n < max) {
                    max = n;
                    maxIndex = i;
                }
            }
            scores[maxIndex] += 1;
        }
        for (int i = 0; i < candidats.length; i++) {
            resultat[i].setScore((float) scores[i] / electeurs.length);
        }
        Arrays.parallelSort(resultat);
        verifierEgalite();
    }

}
