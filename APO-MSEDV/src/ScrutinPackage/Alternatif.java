package ScrutinPackage;

import java.util.Arrays;
import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

public final class Alternatif extends Scrutin {

    public Alternatif(Candidat[] candidats, Electeur[] electeurs) {
        super(candidats, electeurs);
    }

    @Override
    public void CalculateScrutin() {

        resultat = candidats;
        Borda scrut = new Borda(candidats, electeurs);
        for (int i = 0; i < candidats.length -1; i++) {
            scrut.SetNewValue(Arrays.copyOf(resultat, resultat.length - i), electeurs);
            scrut.CalculateScrutin();
            System.arraycopy(scrut.getResultat(), 0, resultat, 0, resultat.length - i);
        }
    }

    @Override
    public String displayResult(){
        String res = super.displayResult();
        res += "Le score est celui du tour auquel le candidat a été éliminé : le dernier, son score au 1er tour ...";
        return res;
    }

}
