package ScrutinPackage;

import java.util.HashMap;
import java.util.Arrays;
import PersonnesPackage.Candidat;
import PersonnesPackage.Electeur;

public final class Approbation extends Scrutin {

    public Approbation(Candidat[] candidats, Electeur[] electeurs) {
        super(candidats, electeurs);
    }

    @Override
    public void CalculateScrutin() {
        resultat = candidats;
        HashMap<String, Float> scoresActuels = new HashMap<String, Float>();
        for (int i = 0; i < candidats.length; i++) {
            resultat[i].setScore(0);
        }
        for (int j = 0; j < electeurs.length; j++) {
            scoresActuels.clear();
            for (int i = 0; i < candidats.length; i++) {
                scoresActuels.put(candidats[i].getNom(), (float) candidats[i].getScore());
                resultat[i].setScore(electeurs[j].norme(candidats[i]));
            }
            Arrays.parallelSort(resultat);
            for (int i = 0; i < resultat.length; i++) {
                if (resultat[i].getScore() < 0.25)
                    resultat[i].setScore((float) scoresActuels.get(resultat[i].getNom()) +resultat.length-i);
                else
                    resultat[i].setScore((float) scoresActuels.get(resultat[i].getNom()));
            }
        }
        Arrays.parallelSort(resultat);
        verifierEgalite();
        int somme = 0;
        
        for(int i = 0; i < resultat.length; i++){
            somme += resultat[i].getScore();
        }
        for(int i = 0; i < resultat.length; i++){
            resultat[i].setScore(resultat[i].getScore()/somme);
        }

    }

}
